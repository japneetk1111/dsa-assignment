public class LinkedList {
    private class Node {
        int data = 0;
        Node next = null;

        Node(int data) {
            this.data = data;
        }
    }

    private Node head = null;
    private Node tail = null;
    private int sizeOfLL = 0;

    public boolean isEmpty() {
        return this.sizeOfLL == 0;
    }

    public int size() {
        return this.sizeOfLL;
    }

    public void display() {
        Node curr = this.head;
        while(curr != null) {
            System.out.print(curr.data + "->");
            curr = curr.next;
        }
        System.out.println();
    }

    private void EmptyException() throws Exception {
        if(this.sizeOfLL == 0) 
            throw new Exception("LinkedList is Empty");
    }

    private void IndexOutOfBoundSizeExclusiveException(int idx) throws Exception {
        if(idx < 0 || idx >= this.sizeOfLL) 
            throw new Exception("Index out of bound");
    }

    private void IndexOutOfBoundSizeInclusiveException(int idx) throws Exception {
        if(idx < 0 || idx > this.sizeOfLL) 
            throw new Exception("Index out of bound");
    }

    private Node getNodeAt(int idx) {
        Node curr = this.head;
        while(idx-- > 0) {
            curr = curr.next;
        }
        return curr;
    }

    public int getAt(int idx) throws Exception {
        IndexOutOfBoundSizeExclusiveException(idx); 
        Node node = getNodeAt(idx);
        return node.data;
    }

    private void insertFirstNode(Node node) {
        if(this.head == null) {
            this.head = this.tail = node;
        } else {
            node.next = this.head;
            this.head = node;
        }
        this.sizeOfLL++;
    }

    public void insertFirst(int data) {
        Node node = new Node(data);
        insertFirstNode(node);
    }

    private void insertLastNode(Node node) {
        if(this.head == null) {
            this.head = this.tail = node;
        } else {
            this.tail.next = node;
            this.tail = node;
        }
        this.sizeOfLL++;
    }

    public void insertLast(int data) {
        Node node = new Node(data);
        insertLastNode(node);
    }
  
    private void insertAtNode(int idx, Node node) {
        if(idx == 0) {
            insertFirstNode(node);
        } else if(idx == this.sizeOfLL) {
            insertLastNode(node);
        } else {
            Node prev = getNodeAt(idx - 1);
            Node forw = prev.next;
            prev.next = node;
            node.next = forw;

            this.sizeOfLL++;
        }
    }

    public void insertAt(int idx, int data) throws Exception {
        IndexOutOfBoundSizeInclusiveException(idx);
        Node node = new Node(data);
        insertAtNode(idx, node);
    }

    private Node deleteFirstNode() {
        Node deleteNode = this.head;
        if(this.sizeOfLL == 1) {
            this.head = this.tail = null;
        } else {
            Node forw = this.head.next;
            deleteNode.next = null;
            this.head = forw;
        }
        this.sizeOfLL--;
        return deleteNode;
    }

    public int deleteFirst() throws Exception {
        EmptyException();
        Node node = deleteFirstNode();
        return node.data;
    }

    private Node deleteLastNode() {
        Node deleteNode = this.tail;
        if(this.sizeOfLL == 1) {
            this.head = this.tail = null;
        } else {
            Node secondLastNode = getNodeAt(this.sizeOfLL - 2);
            this.tail = secondLastNode;
            this.tail.next = null;
        }
        this.sizeOfLL--;
        return deleteNode;
    }

    public int deleteLast() throws Exception {
        EmptyException();
        Node node = deleteLastNode();
        return node.data;
    }

    private Node deleteNodeAt(int idx) {
        if(idx == 0) {
            return deleteFirstNode();
        } else if(idx == this.sizeOfLL - 1) {
            return deleteLastNode();
        } else {
            Node prev = getNodeAt(idx - 1);
            Node curr = prev.next;
            Node forw = curr.next;
            prev.next = forw;
            curr.next = null;

            this.sizeOfLL--;

            return curr;
        }
    }

    public int deleteAt(int idx, int data) throws Exception {
        EmptyException();
        IndexOutOfBoundSizeExclusiveException(idx);
        Node node = deleteNodeAt(idx);
        return node.data;
    }

    public Node reverseList() {
        Node curr = head;
        Node prev = null;
        
        while(curr != null){
            Node forw = curr.next;            
            curr.next = prev;            
            prev = curr;
            curr = forw;
        }
        // display();
        curr = prev;
        while(curr != null) {
            System.out.print(curr.data + "->");
            curr = curr.next;
        }
        System.out.println();
        curr = head;
        return prev;
    }

    public Node centerNodeFirst() {
        Node slow = head;
        Node fast = head;
        while(fast.next != null && fast.next.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        System.out.println(slow.data);
        return slow;
    }

    public Node centerNodeSecond() {
        Node slow = head;
        Node fast = head;
        while(fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        System.out.println(slow.data);
        return slow;
    }
}
