public class LinkedListMain {
    public static void main(String[] args) throws Exception {
        LinkedList ll = new LinkedList();

        for(int i = 1; i <= 10; i++) 
            ll.insertLast(i * 10);

        ll.display();
        ll.insertFirst(200);
        ll.display();
        ll.insertLast(300);
        ll.display();
        ll.insertAt(3, 400);
        ll.display();
        ll.deleteFirst();
        ll.display();
        ll.deleteLast();
        ll.display();
        ll.deleteAt(2, 50);
        ll.display();
        ll.centerNodeFirst();
        ll.centerNodeSecond();
        ll.reverseList();
        ll.display();
    }
}