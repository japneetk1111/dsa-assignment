import java.util.*;

public class NthChildTree {
    public static class Node {
        int data = 0;
        ArrayList<Node> childs = new ArrayList();;

        Node(int data) {
            this.data = data;
        }
    }

    public static boolean find(Node node, int data) {
        boolean res = node.data == data;
        for (Node child : node.childs) {
            res = res || find(child, data);
        }

        return res;
    }

    public static Node addInGT(int[] arr) {
        Node root = null;

        Stack<Node> st = new Stack<>();
        for (int i = 0; i < arr.length; i++) {
        if (arr[i] == -1) {
            st.pop();
        } else {
            Node t = new Node(arr[i]);
            if (st.size() > 0) {
            st.peek().childs.add(t);
        } else {
          root = t;
        }

        st.push(t);
      }
    }

    return root;
  }

  public static Queue<Node> q = new ArrayDeque<>();
  public static void BFS(Node node){
    if(node == null){//no tree
      return;
    }

    Node temp = q.remove();
    System.out.print(temp.data + " ");
    for(Node child : temp.childs){
      q.add(child);
    }

    if(q.size() != 0){
      BFS(q.peek());
    }else{
      System.out.println(".");
      return;
    }

  }

    public static void DFS(Node node) {
        if (node == null)
            return;
 
        int total = node.childs.size();
        for (int i = 0; i < total - 1; i++)
            DFS(node.childs.get(i));
 
        System.out.print("" + node.data + " ");
 
        DFS(node.childs.get(total - 1));
    }

    public static void display(Node node) {
        System.out.print(node.data + " -> ");
        for (Node child : node.childs) {
            System.out.print(child.data + ", ");
        }
        System.out.println();

        for (Node child : node.childs)
            display(child);
    }

}