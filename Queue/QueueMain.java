public class QueueMain {
    public static void main(String[] args) {
        Queue que = new Queue();

        for (int i = 0; i < 15; i++)
            que.enque(i * 100);

        que.display();
        que.enque(750);
        System.out.println(que.deque());
        que.display();
        System.out.println(que.peek());
        System.out.println(que.size());
        boolean res = que.contains(700);
        if(res) System.out.println("Present");
        else System.out.println("Not Present");
        que.display();
    }
}