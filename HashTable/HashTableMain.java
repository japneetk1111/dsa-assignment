import java.util.Scanner;
import java.util.ArrayList;

public class HashTableMain {
    public static Scanner scn = new Scanner(System.in);

    public static void main(String[] args) {
        HashTable<Character, ArrayList<Integer>> map = new HashTable<>();
        String str = "asdasasaaasskacbasmcbasmcasbcjhcbsahcjhcsvhcsHVcsvhjcks";
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            map.putIfAbsent(ch, new ArrayList<>());
            map.get(ch).add(i);
        }

        for(Character ch : map.keySet()){
            System.out.println(ch + " -> " + map.get(ch));
        }
    }
}