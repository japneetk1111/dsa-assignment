public class StackMain {
    public static void main(String[] args) throws Exception {
        Stack st = new Stack(15);
        for (int i = 1; i <= 5; i++)
            st.push(i * 10);

        st.display();
        st.push(60);
        st.display();
        st.pop();
        st.display();
        System.out.println(st.top());
        System.out.println(st.size());
        boolean res = st.contains(40);
        if(res) System.out.println("Present");
        else System.out.println("Not Present");
        st.display();
    }
}