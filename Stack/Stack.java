public class Stack {
    protected int[] arr = null;
    protected int capacity = 0; 
    protected int elementCount = 0; 
    protected int tos = -1;

    // constructor.=============================================

    protected void intializeVariables(int capacity) {
        this.capacity = capacity;
        this.arr = new int[this.capacity];
        this.elementCount = 0;
        this.tos = -1;
    }

    public Stack() {
        intializeVariables(10); 
    }

    public Stack(int size) {
        intializeVariables(size);
    }

    public int size() {
        return this.elementCount;
    }

    public boolean isEmpty() {
        return this.elementCount == 0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < this.elementCount; i++) {
            sb.append(this.arr[i]);
            if (i != this.elementCount - 1)
                sb.append(", ");
        }
        sb.append("]");
        return sb.toString();
    }

    public void display() {
        System.out.println(toString());
    }

    private void OverflowException() throws Exception {
        if (this.capacity == this.elementCount) {
            throw new Exception("StackIsFull");
        }
    }

    private void underFlowException() throws Exception {
        if (this.elementCount == 0) {
            throw new Exception("StackIsEmpty");
        }
    }

    protected void push_(int data) {
        this.arr[++this.tos] = data;
        this.elementCount++;
    }

    public void push(int data) throws Exception {
        OverflowException();
        push_(data);
    }

    public int top() throws Exception {
        underFlowException();
        return this.arr[this.tos];
    }

    protected int pop_() {
        int rv = this.arr[this.tos];
        this.arr[this.tos--] = 0;
        this.elementCount--;
        return rv;
    }

    public int pop() throws Exception {
        underFlowException();
        return pop_();
    }

    public boolean contains(int key) {
        for(int i = 0; i < elementCount; i++) {
            if(arr[i] == key) return true;
        }
        return false;
    }
}